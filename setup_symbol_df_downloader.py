from distutils.core import setup
from Cython.Build import cythonize
setup( ext_modules = cythonize("symbol_df_downloader_cython.pyx") )
