#기본
import json
import time
import pprint
import math
import datetime
import os
import csv

# 데이터베이스
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
##########################기본작업##########################
pp = pprint.PrettyPrinter(indent=4)
result_folder = "result_f"
#########################db 모델#########################
app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///symbol_data.db'
app.config['SQLALCHEMY_BINDS'] = {
        'result_db':'sqlite:///result.db' 
        }
db = SQLAlchemy(app)

class ResultData(db.Model):
        #최종결과 데이터베이스
    __bind_key__ = 'result_db'
    id = db.Column(db.Integer, primary_key=True)
    symbol = db.Column(db.String(20))
    martin_tick = db.Column(db.Float(20))
    reverse = db.Column(db.Boolean)
    drawdown = db.Column(db.Float(20))
    martin_count = db.Column(db.Integer)
    result_count = db.Column(db.Integer)
    need_bal = db.Column(db.Float)
    
    def __repr__(self):
        return '<ResultData %r>' % self.symbol
####################################메인 프로세싱####################################
if not os.path.isdir(result_folder):
    os.mkdir(result_folder)
all_symbol_list = (lambda x: list(set([i.symbol for i in x])))(ResultData.query.order_by(ResultData.symbol).all())


for _symbol in all_symbol_list :
    symbol_data_list = ResultData.query.filter_by(symbol=_symbol).all()
    if "/" in _symbol :
        symbol_name = _symbol.replace("/","")
    else: symbol_name = _symbol
    csv_file = open(f'{result_folder}/{symbol_name}.csv', mode="w", encoding="utf-8", newline="")
    csv_writter = csv.writer(csv_file)
    csv_writter.writerow(["martin_tick","reverse","drawdown","martin_count","result_count","need_bal"])
    for symbol_data in symbol_data_list :
        martin_tick = symbol_data.martin_tick
        reverse = symbol_data.reverse
        drawdown = symbol_data.drawdown
        martin_count = symbol_data.martin_count
        result_count = symbol_data.result_count
        need_bal = symbol_data.need_bal
        csv_writter.writerow([
            martin_tick,
            reverse,
            drawdown,
            martin_count,
            result_count,
            need_bal
        ])
    csv_file.close()
