'''
db 테스트 파일로 지정되어 있음
all_date_list = all_date_list[-100:] 삭제필요 103번쯤
수정필요 표시됨
'''
#########################모듈#########################
#기본
import json
import time
import pprint
import math
import datetime
import os

# 데이터베이스
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# 멀티프로세싱
import multiprocessing
import parmap
##########################기본작업##########################
pp = pprint.PrettyPrinter(indent=4)
#########################db 모델#########################
app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///symbol_data.db'
app.config['SQLALCHEMY_BINDS'] = {
        # 'df_db':'sqlite:///symbol_data.db',
        'df_db':'sqlite:///symbol_data_test.db', #수정필요
        'result_db':'sqlite:///result.db' 
        # 'result_db':'sqlite:///result_test.db'
        }
db = SQLAlchemy(app)

# json db
class SymbolData(db.Model):
    __bind_key__ = 'df_db'
    id = db.Column(db.Integer, primary_key=True)
    symbol = db.Column(db.String(20))
    date = db.Column(db.DateTime)
    open_price = db.Column(db.Float(20))
    high_price = db.Column(db.Float(20))
    low_price = db.Column(db.Float(20))
    close_price = db.Column(db.Float(20))
    volume = db.Column(db.Float(20))
    change = db.Column(db.Float(20))
    
    def __repr__(self):
        return '<SymbolData %r>' % self.symbol
class ResultData(db.Model):
        #최종결과 데이터베이스
    __bind_key__ = 'result_db'
    id = db.Column(db.Integer, primary_key=True)
    symbol = db.Column(db.String(20))
    martin_tick = db.Column(db.Float(20))
    reverse = db.Column(db.Boolean)
    drawdown = db.Column(db.Float(20))
    martin_count = db.Column(db.Integer)
    result_count = db.Column(db.Integer)
    need_bal = db.Column(db.Float)
    
    def __repr__(self):
        return '<ResultData %r>' % self.symbol
if not os.path.isfile("result.db"):
    db.create_all(bind="result_db")
#########################클래스 및 함수#########################
# 최소틱 최대틱 찾기
def find_min_max_tick_size(db_table, all_date_list, _symbol):
    #여기서부터 최소틱 사이즈 구하기
    # average_5_date_list = [] # 총 날짜의 첫일 + 평균4일 + 마지막 날짜만 모으기
    average_5_date_close_list = [] #평균일자들의 종가 리스트
    for _date in all_date_list :
        for multiply_n in range(0,5) :
            if (_date == all_date_list[math.floor(len(all_date_list)/5*multiply_n)]
            or _date == all_date_list[-1]) : # 모든 날짜의 첫일 평균4일 날짜인지 + 마지막 날짜인지
                # average_5_date_list.append(_date)
                _date_date = db_table.query.filter_by(symbol=_symbol,date=_date).first() #평균일자의 db데이터
                _close = _date_date.close_price #평균일자의 종가
                average_5_date_close_list.append(_close)
                if _date == all_date_list[-1] : break # 마지막 일자의 경우 multiply_n 루프 한번만 하고 정지
    max_int_len = 0 #정수 최대 자릿수
    max_float_len = 0 #소수 최대 자릿수
    for _date_close in average_5_date_close_list : #최소틱 최대틱 찾기
        int_float_list = str(_date_close).split(".") #split함수로 정수자리 소수자리 리스트로 나누기
        if int_float_list[1] == "0" : #소수자리가 0일경우
            int_float_list[1] = "" #소수자리는 없애줌
        int_len = len(int_float_list[0]) #정수자릿수
        float_len = len(int_float_list[1]) #소수자릿수
        if max_int_len < int_len : #현재 정수 자릿수가 정수 최대 자릿수보다 큰경우
            max_int_len = int_len
        if max_float_len < float_len : #현재 소수 자릿수가 소수 최대 자릿수보다 큰경우
            max_float_len = float_len
    min_tick = round(0.1**max_float_len,max_float_len) #최소틱 = 0.1 ** 소수 최대 자릿수(예: 소수최대자릿수가 3의경우 최소틱은 0.001이됨) / 내림안하면 파이썬 오류로 너무 아랫자리까지 표시되서 내림해줌
    max_tick = 10**max_int_len #최대틱 예: 최대 가격이 100.01이라고 가정->1000까지 가격이 있을 수 있다고 가정함->10**3 = 1000, 최대틱은 1000틱으로 가정
    # 여기까지 최소틱 최대틱 찾기    
    return [min_tick, max_tick]

#최소 틱부터 수익을 낼 수 없는 수준의 틱까지 마틴게일 계산
def cal_martin(db_table, all_date_list, _symbol, min_max_tick_size) :
    min_tick = min_max_tick_size[0]
    max_tick = min_max_tick_size[1]
    # 여기서부터 각 틱 별로 each tick dict 구성
    each_tick_dict = {} #최소틱부터 최대틱까지 최소틱단위로 나누어서 과정 및 결과값 입력
    for _tick in range(100,int((max_tick/min_tick)+1)): #100틱 부터 1틱기준 최대틱까지
        tick_dict = {} #개별 틱에 들어갈 딕셔너리 구성
        tick_dict["price_for_cal"] = {
            "martin_high":0.0,
            "martin_low":0.0,
            "martin_high_date":0.0,
            "martin_low_date":0.0
            } #연산에 필요한 가격정보 [마틴 전 최고가, 마틴 중 최저가]
        tick_dict["high_in_proc"] = {} # 마틴저가 날짜와 상관없이 청산이전까지의 고가 모두 입력 -> 이후 마틴저가날짜보다 이전이면서 최고가 찾아서 마틴고가와 변경
        tick_dict["result"] = {0:{"martin_high":0.0,"martin_low":0.0,"martin_high_date":0.0,"martin_low_date":0.0,"martin_close":0.0,"martin_close_date":0.0,"martin_drawdown":0.0}} # 각 마틴마다 얼마만큼 하락했었는지 결과 #마틴고가 마틴저가 마틴종가 각각 날짜 하락폭 포함
        each_tick_dict[_tick*min_tick] = tick_dict
        #여기까지 각 틱 별로 each tick dict 구성
    ''' 
    연산계획: 
    첫 데이터의 경우 
    마틴전 최고가=고가, 마틴중 최저가=저가, 마틴중 최종가=종가
    첫데이터 이후
    성공실패기준: (현재고가)-(이전 마틴중최저가) > 마틴틱*2, (현재종가)-(현재 마틴중최저가) > 마틴틱*2
    마틴게일 성공시: 마틴전 최고가= 직전종가와 현재고가 중 더 높은 가격, 마틴중 최저가=저가, 마틴최종가=종가
    마틴게일 실패시: 마틴전 최고가= 이전 마틴전최고가 그대로, 마틴중최저가=저가, 마틴최종가=종가
    '''
    martin_tick_list = list(each_tick_dict.keys())
    # all_date_list = all_date_list[-550:] #수정필요
    def main_cal(each_tick_dict, martin_tick_list, all_date_list, db_table, _symbol, reverse):
        '''여기서부터 상위함수 변수 카피'''
        each_tick_dict_copy = each_tick_dict.copy()
        count_date = 0
        for _date in all_date_list :#각 일자별 for
            symbol_this_date_data = db_table.query.filter_by(symbol=_symbol,date=_date).first() #해당심볼 해당일자 db데이터
            if reverse==True:
                    # 공매도 계산시 사용
                open_price = -symbol_this_date_data.open_price
                low_price = -symbol_this_date_data.high_price
                high_price = -symbol_this_date_data.low_price
                close_price = -symbol_this_date_data.close_price
                    # 가격을 뒤집으면 일일기준 저가->고가, 고가->저가 됨으로 서로 바꿔줌
            else:
                open_price = symbol_this_date_data.open_price
                high_price = symbol_this_date_data.high_price
                low_price = symbol_this_date_data.low_price
                close_price = symbol_this_date_data.close_price
            for martin_tick in martin_tick_list: # 여기서부터 각 마틴 틱마다 별도 계산
                tick_dict = each_tick_dict_copy[martin_tick] # 해당마틴틱 계산 및 결과 딕트
                if all_date_list[0] == _date : # 첫데이터의 경우
                    tick_dict["price_for_cal"]["martin_high"]=high_price
                    tick_dict["price_for_cal"]["martin_high_date"]=_date
                    if (close_price - low_price) > (martin_tick*2) : #저가를 찍고 종가갔는데 마틴청산한 경우
                        tick_dict["result"][max(list(tick_dict["result"].keys()))+1]={
                            "martin_high":high_price,
                            "martin_low":low_price,
                            "martin_close":close_price,
                            "martin_high_date":_date,
                            "martin_low_date":_date,
                            "martin_close_date":_date,
                            "martin_drawdown":(high_price-low_price)
                        }
                        tick_dict["price_for_cal"]["martin_low"]=close_price
                        tick_dict["price_for_cal"]["martin_low_date"]=_date # 마틴청산 결과 삽입 후 마틴저가를 종가로 설정(시가->고가->저가->종가 가정)
                    else:
                        tick_dict["price_for_cal"]["martin_low"]=low_price
                        tick_dict["price_for_cal"]["martin_low_date"]=_date # 첫데이터에서 마틴청산이 없는 경우 마틴저가를 현재저가로 설정
                else: #첫데이터가 아닌경우
                    if (high_price - tick_dict["price_for_cal"]["martin_low"]) > (martin_tick*2): #이전마틴저가로 부터 현재고가가 마틴틱보다 큰경우(시가->고가 가정))
                        '''여기서부터 마틴고가 갱신'''
                        if len(tick_dict["high_in_proc"]) >0 : #첫 마틴고가 이후 고가갱신이 1번이라도 있었던 경우
                            high_in_proc_under_martin_low_date = dict(filter(lambda x: x[0]<=tick_dict["price_for_cal"]["martin_low_date"], tick_dict["high_in_proc"].items()))
                                # high_in_proc 딕셔너리 내 마틴저가 날짜보다 이전의 날짜 필터링
                            if len(high_in_proc_under_martin_low_date) >0 :#마틴저가날짜 이전일자 중 고가갱신이 있었던 경우
                                highest_in_proc_under_martin_low_date = sorted(high_in_proc_under_martin_low_date.items(), key=lambda x: x[1], reverse=True)
                                    # 가격이 높은 순으로 정렬
                                if highest_in_proc_under_martin_low_date[0][1] > tick_dict["price_for_cal"]["martin_high"]:
                                        #기존의 마틴고가보다 이후 마틴저가일자 이전 일자 최고가가 높은 경우
                                    tick_dict["price_for_cal"]["martin_high"] = highest_in_proc_under_martin_low_date[0][1]
                                    tick_dict["price_for_cal"]["martin_high_date"] = highest_in_proc_under_martin_low_date[0][0]
                            tick_dict["high_in_proc"] = {}
                        '''여기까지 마틴고가 갱신'''
                        tick_dict["result"][max(list(tick_dict["result"].keys()))+1]={
                            "martin_high":tick_dict["price_for_cal"]["martin_high"],
                            "martin_low":tick_dict["price_for_cal"]["martin_low"],
                            "martin_close":high_price,
                            "martin_high_date":tick_dict["price_for_cal"]["martin_high_date"],
                            "martin_low_date":tick_dict["price_for_cal"]["martin_low_date"],
                            "martin_close_date":_date,
                            "martin_drawdown":(tick_dict["price_for_cal"]["martin_high"] - tick_dict["price_for_cal"]["martin_low"])
                        }
                        tick_dict["price_for_cal"]["martin_high"]=high_price # 고가에서 청산됐다고 가정했으므로 새로운 마틴고가는 현재고가로 지정
                        tick_dict["price_for_cal"]["martin_high_date"]=_date
                        tick_dict["price_for_cal"]["martin_low"]=low_price
                        tick_dict["price_for_cal"]["martin_low_date"]=_date  # 이후 고가에서 저가로 하락했다고 가정했으므로 새로운 마틴저가는 현재저가로 지정
                        if (close_price - tick_dict["price_for_cal"]["martin_low"]) > (martin_tick*2): # 현재마틴저가로부터 현재종가가 마틴틱보다 큰경우(시->고->저->종->고->종 가정)
                            tick_dict["result"][max(list(tick_dict["result"].keys()))+1]={
                                "martin_high":tick_dict["price_for_cal"]["martin_high"],
                                "martin_low":tick_dict["price_for_cal"]["martin_low"],
                                "martin_close":close_price,
                                "martin_high_date":tick_dict["price_for_cal"]["martin_high_date"],
                                "martin_low_date":tick_dict["price_for_cal"]["martin_low_date"],
                                "martin_close_date":_date,
                                "martin_drawdown":(tick_dict["price_for_cal"]["martin_high"] - tick_dict["price_for_cal"]["martin_low"])
                            }
                            # tick_dict["price_for_cal"]["martin_high"]=high_price # 종가에서 청산했다고 가정-> 이후 고가로 상승했다고 가정-> 마틴고가=고가
                            # tick_dict["price_for_cal"]["martin_high_date"]=_date
                            tick_dict["price_for_cal"]["martin_low"]=close_price # 이후 고가에서 종가로 하락했다고 가정했으므로 새로운 마틴저가는 종가로 지정
                            tick_dict["price_for_cal"]["martin_low_date"]=_date  
                    elif tick_dict["price_for_cal"]["martin_low"] > low_price: # 현재 데이터에서 아무런 청산없이 저가가 이전마틴저가보다 더 낮을 경우
                        tick_dict["price_for_cal"]["martin_low"]=low_price # 저가를 새로운 마틴저가로 지정
                        tick_dict["price_for_cal"]["martin_low_date"]=_date
                    if tick_dict["price_for_cal"]["martin_high"] < high_price: # 청산되기 전에 이전마틴고가보다 고가가 높은 경우 tick_dict["high_in_proc"]에 추가
                        tick_dict["high_in_proc"][_date] = high_price
            count_date += 1
            print("\r",f'reverse: {reverse}, date: {count_date}',end="          ")
        # result_dict = {} #논인버스결과 또는 인버스결과 모음 딕셔너리
        count_martin_tick = 0
        for martin_tick in list(each_tick_dict_copy.keys()) :
            martin_tick_result_dict = each_tick_dict_copy[martin_tick]["result"] #해당마틴틱의 모든결과 딕셔너리
            if len(martin_tick_result_dict) == 1 : # 해당마틴틱의 결과 초반 예시용 0번 밖에 없을 경우
                continue
            highest_dd = 0 #최대 dd 찾기 시작
            for result_n in list(martin_tick_result_dict.keys()):
                drawdown = martin_tick_result_dict[result_n]["martin_drawdown"]
                if highest_dd < drawdown :
                    highest_dd = drawdown #최대 dd 갱신
            martin_count = math.ceil(highest_dd / martin_tick)
            need_bal = martin_tick # 마틴틱별 필요한 자금 계산
            for r1 in range(0,martin_count):
                if r1 != 0 :
                    need_bal += need_bal #마틴 10번, 마틴틱 2 가정 -> 2+4+8+16+32+64+...

            # result_dict[martin_tick] = highest_dd #최대 dd값을 최종 결과 딕셔너리[해당마틴틱] 값에 삽입
            new_result_data = ResultData(symbol=_symbol, martin_tick=martin_tick, reverse=reverse, drawdown=highest_dd, martin_count=martin_count, result_count=len(martin_tick_result_dict)-1, need_bal=need_bal)
            # print(f'{_symbol} {round(martin_tick,1)} {reverse} {highest_dd} {martin_count} {len(martin_tick_result_dict)-1}')
            db.session.add(new_result_data)
            count_martin_tick+=1
            if count_martin_tick % 100 == 0:
                db.session.commit()
            print("\r",f'reverse: {reverse}, martin_tick_count: {count_martin_tick}',end="          ")
        db.session.commit()
        # return result_dict

    # non_inverse_result = main_cal(each_tick_dict, martin_tick_list, all_date_list, db_table, _symbol, False)
    inverse_result = main_cal(each_tick_dict, martin_tick_list, all_date_list, db_table, _symbol, True)
def cal_martin_2(db_table, all_date_list, _symbol, min_max_tick_size) :
    min_tick = min_max_tick_size[0]
    max_tick = min_max_tick_size[1]
    # 여기서부터 각 틱 별로 each tick dict 구성
    each_tick_dict = {} #최소틱부터 최대틱까지 최소틱단위로 나누어서 과정 및 결과값 입력
    for _tick in range(10,int((max_tick/min_tick)+1)): #10틱 부터 1틱기준 최대틱까지
        tick_dict = {} #개별 틱에 들어갈 딕셔너리 구성
        tick_dict["price_for_cal"] = {
            "martin_high":0.0,
            "martin_low":0.0,
            "martin_high_date":0.0,
            "martin_low_date":0.0
            } #연산에 필요한 가격정보 [마틴 전 최고가, 마틴 중 최저가]
        tick_dict["result"] = {0:{"martin_high":0.0,"martin_low":0.0,"martin_high_date":0.0,"martin_low_date":0.0,"martin_close":0.0,"martin_close_date":0.0,"martin_drawdown":0.0}} # 각 마틴마다 얼마만큼 하락했었는지 결과 #마틴고가 마틴저가 마틴종가 각각 날짜 하락폭 포함
        each_tick_dict[_tick*min_tick] = tick_dict
        #여기까지 각 틱 별로 each tick dict 구성
    '''
    <연산계획>
    기준일자 데이터의 고가를 마틴고가
    이후 청산 전까지 최저가를 마틴저가
    이후 청산시 청산가를 마틴종가
    마틴틱 별로 기준일자를 시작으로 청산일자까지 연산
    <결론>
    매일마다 새롭게 고가를 마틴고가로 지정(이후 마틴고가 변함없음) 후 청산하기 전까지 최대하락폭을 찾는 작업을 함(시간이 많이 걸림)
    '''
    count_n = 0 # index_n 날짜 기록
    martin_tick_list = list(each_tick_dict.keys()) # 모든 마틴틱 리스트
    # all_date_list = all_date_list[-100:] #수정필요
    for index_n in range(len(all_date_list)-1): #index_n번째 날짜가 마틴게일 시작일자이므로 마틴게일 다음날은 index_n+1번째 날짜임->그래서 리스트 오류방지를 위해 len(all_date_list)-1
        _date = all_date_list[index_n]
        symbol_this_date_data = db_table.query.filter_by(symbol=_symbol,date=_date).first() #해당심볼 해당일자 db데이터
        open_price = symbol_this_date_data.open_price
        high_price = symbol_this_date_data.high_price
        low_price = symbol_this_date_data.low_price
        close_price = symbol_this_date_data.close_price
        after_first_date_list = all_date_list[index_n+1:] #index_n번째일자 이후의 날짜 리스트
        for martin_tick in martin_tick_list: # 여기서부터 각 마틴 틱마다 별도 계산
            tick_dict = each_tick_dict[martin_tick] # 해당마틴틱 계산 및 결과 딕트
            tick_dict["price_for_cal"]={
            "martin_high":high_price,
            "martin_low":low_price,
            "martin_high_date":_date,
            "martin_low_date":_date
            } # 마틴시작일의 고가를 마틴고가로, 저가를 마틴저가로 지정
            ''' 마틴시작일의 종가는 실제로는 저가를 찍고 종가에서 청산했을 경우가 있을 수 있지만 다음 날 고가가 현재종가보다 낮을 수 있으므로 종가는 무시한다 '''
            for after_date in after_first_date_list :
                symbol_after_date_data = db_table.query.filter_by(symbol=_symbol,date=after_date).first() #해당심볼 마틴시작 이후 날짜 db데이터
                after_high_price = symbol_after_date_data.high_price
                after_low_price = symbol_after_date_data.low_price
                if after_high_price - tick_dict["price_for_cal"]["martin_low"] > martin_tick*2 : #고가-마틴저가 마틴틱*2(마틴저가가 마틴매수가격보다 +1틱 차이로 매수가 안될 수 있으므로 안전하게 *2를 함)보다 큰경우(청산)
                    tick_dict["result"][max(list(tick_dict["result"].keys()))+1]={
                        "martin_high":tick_dict["price_for_cal"]["martin_high"],
                        "martin_low":tick_dict["price_for_cal"]["martin_low"],
                        "martin_close":after_high_price,
                        "martin_high_date":tick_dict["price_for_cal"]["martin_high_date"],
                        "martin_low_date":tick_dict["price_for_cal"]["martin_low_date"],
                        "martin_close_date":after_date,
                        "martin_drawdown":tick_dict["price_for_cal"]["martin_high"] - tick_dict["price_for_cal"]["martin_low"]
                    }
                    break # 마틴청산하였으므로 해당 마틴틱에서의 연산은 제거
                elif after_low_price < tick_dict["price_for_cal"]["martin_low"]: # 마틴저가보다 이번 데이터 저가가 더 낮을 경우
                    tick_dict["price_for_cal"]["martin_low"] = after_low_price # 마틴저가를 갱신
                    tick_dict["price_for_cal"]["martin_low_date"] = after_date # 마틴저가 날짜도 갱신
        count_n += 1
        print("\r",count_n,end="")
    print()
    pp.pprint(each_tick_dict[100.0])
    print(_symbol)

########################메인프로세싱########################
start_t = time.time() #타이머시작
all_symbol_list= (lambda x : sorted(list(set([i.symbol for i in x]))))(SymbolData.query.order_by(SymbolData.symbol).all()) #json db 모든 row 리스트
count_symbol_n = [0]
def multiprocessing_work(all_symbol_list):
    for _symbol in all_symbol_list: #여기서부터 계산시작
        
        # if ResultData.query.filter_by(symbol=_symbol).first() != None: continue # 해당심볼 결과가 이미 존재할 경우 패스
        symbol_data_list = SymbolData.query.filter_by(symbol=_symbol).all() #해당 심볼 모든 일자별 데이터
        all_date_list = (lambda x: sorted(list([i.date for i in x])))(symbol_data_list) # 해당 심볼 모든 일자(순서대로)
        min_max_tick_size_list = find_min_max_tick_size(db_table=SymbolData, all_date_list=all_date_list, _symbol=_symbol) # [min_tick_size, max_tick_size]
        martin_result = cal_martin(db_table=SymbolData, all_date_list=all_date_list, _symbol=_symbol, min_max_tick_size=min_max_tick_size_list) #마틴게일 최적화 계산
        count_symbol_n[0] += 1
        print("\r",f'symbol count: {str(count_symbol_n[0]).zfill(6)}  symbol: {_symbol}          ')
# num_cores = multiprocessing.cpu_count() # 8
# chunk_size = math.ceil(len(all_symbol_list) / num_cores)
# splited_symbol_list = [all_symbol_list[i * chunk_size:(i + 1) * chunk_size] for i in range((len(all_symbol_list) - 1 + chunk_size) // chunk_size )] 
# multi_work = parmap.map(multiprocessing_work, splited_symbol_list, pm_pbar=True, pm_processes=5)
# multiprocessing_work(all_symbol_list[:2]) #수정필요
# multiprocessing_work(["EUR/USD,None"])
# all_symbol_list.remove("EUR/USD,None")
multiprocessing_work(all_symbol_list)
    

print(f'used time: {time.time()-start_t}') #타이머 끝
