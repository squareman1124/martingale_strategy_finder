'''
db 테스트 파일로 지정되어 있음
all_date_list = all_date_list[-100:] 삭제필요 103번쯤
수정필요 표시됨
'''
#########################모듈#########################
#기본
import json
import time
import pprint
import math
import datetime
import os
import csv
#멀티프로세싱
import multiprocessing
import parmap
##########################기본작업##########################
pp = pprint.PrettyPrinter(indent=4)
data_folder = "symbol_data_f"
result_folder = "result_f"
#########################클래스 및 함수#########################

# 최소틱 최대틱 찾기
def find_min_max_tick_size(csv_dict_data, all_date_list):
    #여기서부터 최소틱 사이즈 구하기
    # average_5_date_list = [] # 총 날짜의 첫일 + 평균4일 + 마지막 날짜만 모으기
    average_5_date_close_list = [] #평균일자들의 종가 리스트
    for _date in all_date_list :
        for multiply_n in range(0,5) :
            if (_date == all_date_list[math.floor(len(all_date_list)/5*multiply_n)]
            or _date == all_date_list[-1]) : # 모든 날짜의 첫일 평균4일 날짜인지 + 마지막 날짜인지
                # average_5_date_list.append(_date)
                _date_data = csv_dict_data[_date]
                _close = _date_data["close"] #평균일자의 종가
                average_5_date_close_list.append(_close)
                if _date == all_date_list[-1] : break # 마지막 일자의 경우 multiply_n 루프 한번만 하고 정지
    max_int_len = 0 #정수 최대 자릿수
    max_float_len = 0 #소수 최대 자릿수
    for _date_close in average_5_date_close_list : #최소틱 최대틱 찾기
        int_float_list = str(_date_close).split(".") #split함수로 정수자리 소수자리 리스트로 나누기
        if int_float_list[1] == "0" : #소수자리가 0일경우
            int_float_list[1] = "" #소수자리는 없애줌
        int_len = len(int_float_list[0]) #정수자릿수
        float_len = len(int_float_list[1]) #소수자릿수
        if max_int_len < int_len : #현재 정수 자릿수가 정수 최대 자릿수보다 큰경우
            max_int_len = int_len
        if max_float_len < float_len : #현재 소수 자릿수가 소수 최대 자릿수보다 큰경우
            max_float_len = float_len
    min_tick = round(0.1**max_float_len,max_float_len)*10 #최소틱 = 0.1 ** 소수 최대 자릿수(예: 소수최대자릿수가 3의경우 최소틱은 0.001이됨) / 내림안하면 파이썬 오류로 너무 아랫자리까지 표시되서 내림해줌
    max_tick = 10**max_int_len #최대틱 예: 최대 가격이 100.01이라고 가정->1000까지 가격이 있을 수 있다고 가정함->10**3 = 1000, 최대틱은 1000틱으로 가정
    # 여기까지 최소틱 최대틱 찾기    
    return [min_tick, max_tick]

#최소 틱부터 수익을 낼 수 없는 수준의 틱까지 마틴게일 계산
def cal_martin(csv_dict_data, all_date_list, result_file_path, min_max_tick_size) :
    min_tick = min_max_tick_size[0]
    max_tick = min_max_tick_size[1]
    # 여기서부터 각 틱 별로 each tick dict 구성
    each_tick_dict = {} #최소틱부터 최대틱까지 최소틱단위로 나누어서 과정 및 결과값 입력
    for _tick in range(10,int((max_tick/min_tick)+1)): #100틱 부터 1틱기준 최대틱까지
        tick_dict = {} #개별 틱에 들어갈 딕셔너리 구성
        tick_dict["price_for_cal"] = {
            "martin_high":0.0,
            "martin_low":0.0,
            "martin_high_date":0.0,
            "martin_low_date":0.0
            } #연산에 필요한 가격정보 [마틴 전 최고가, 마틴 중 최저가]
        tick_dict["high_in_proc"] = {} # 마틴저가 날짜와 상관없이 청산이전까지의 고가 모두 입력 -> 이후 마틴저가날짜보다 이전이면서 최고가 찾아서 마틴고가와 변경
        tick_dict["result"] = {0:{"martin_high":0.0,"martin_low":0.0,"martin_high_date":0.0,"martin_low_date":0.0,"martin_close":0.0,"martin_close_date":0.0,"martin_drawdown":0.0}} # 각 마틴마다 얼마만큼 하락했었는지 결과 #마틴고가 마틴저가 마틴종가 각각 날짜 하락폭 포함
        each_tick_dict[_tick*min_tick] = tick_dict
        #여기까지 각 틱 별로 each tick dict 구성
    ''' 
    연산계획: 
    첫 데이터의 경우 
    마틴전 최고가=고가, 마틴중 최저가=저가, 마틴중 최종가=종가
    첫데이터 이후
    성공실패기준: (현재고가)-(이전 마틴중최저가) > 마틴틱*2, (현재종가)-(현재 마틴중최저가) > 마틴틱*2
    마틴게일 성공시: 마틴전 최고가= 직전종가와 현재고가 중 더 높은 가격, 마틴중 최저가=저가, 마틴최종가=종가
    마틴게일 실패시: 마틴전 최고가= 이전 마틴전최고가 그대로, 마틴중최저가=저가, 마틴최종가=종가
    '''
    martin_tick_list = list(each_tick_dict.keys())
    # all_date_list = all_date_list[-550:] #수정필요
    def main_cal(each_tick_dict, martin_tick_list, all_date_list, csv_dict_data, result_file_path, reverse, result_csv):
        '''여기서부터 상위함수 변수 카피'''
        each_tick_dict_copy = each_tick_dict.copy()
        count_date = 0
        for _date in all_date_list :#각 일자별 for
            try: #계속 killed 당해서 한번 시도
                symbol_this_date_data = csv_dict_data[_date] #해당심볼 해당일자 db데이터
                if reverse==True:
                        # 공매도 계산시 사용
                    open_price = -symbol_this_date_data["open"]
                    low_price = -symbol_this_date_data["high"]
                    high_price = -symbol_this_date_data["low"]
                    close_price = -symbol_this_date_data["close"]
                        # 가격을 뒤집으면 일일기준 저가->고가, 고가->저가 됨으로 서로 바꿔줌
                else:
                    open_price = symbol_this_date_data["open"]
                    high_price = symbol_this_date_data["high"]
                    low_price = symbol_this_date_data["low"]
                    close_price = symbol_this_date_data["close"]
                for martin_tick in martin_tick_list: # 여기서부터 각 마틴 틱마다 별도 계산
                    tick_dict = each_tick_dict_copy[martin_tick] # 해당마틴틱 계산 및 결과 딕트
                    if all_date_list[0] == _date : # 첫데이터의 경우
                        tick_dict["price_for_cal"]["martin_high"]=high_price
                        tick_dict["price_for_cal"]["martin_high_date"]=_date
                        if (close_price - low_price) > (martin_tick*2) : #저가를 찍고 종가갔는데 마틴청산한 경우
                            tick_dict["result"][max(list(tick_dict["result"].keys()))+1]={
                                "martin_high":high_price,
                                "martin_low":low_price,
                                "martin_close":close_price,
                                "martin_high_date":_date,
                                "martin_low_date":_date,
                                "martin_close_date":_date,
                                "martin_drawdown":(high_price-low_price)
                            }
                            tick_dict["price_for_cal"]["martin_low"]=close_price
                            tick_dict["price_for_cal"]["martin_low_date"]=_date # 마틴청산 결과 삽입 후 마틴저가를 종가로 설정(시가->고가->저가->종가 가정)
                        else:
                            tick_dict["price_for_cal"]["martin_low"]=low_price
                            tick_dict["price_for_cal"]["martin_low_date"]=_date # 첫데이터에서 마틴청산이 없는 경우 마틴저가를 현재저가로 설정
                    else: #첫데이터가 아닌경우
                        if (high_price - tick_dict["price_for_cal"]["martin_low"]) > (martin_tick*2): #이전마틴저가로 부터 현재고가가 마틴틱보다 큰경우(시가->고가 가정))
                            '''여기서부터 마틴고가 갱신'''
                            if len(tick_dict["high_in_proc"]) >0 : #첫 마틴고가 이후 고가갱신이 1번이라도 있었던 경우
                                high_in_proc_under_martin_low_date = dict(filter(lambda x: x[0]<=tick_dict["price_for_cal"]["martin_low_date"], tick_dict["high_in_proc"].items()))
                                    # high_in_proc 딕셔너리 내 마틴저가 날짜보다 이전의 날짜 필터링
                                if len(high_in_proc_under_martin_low_date) >0 :#마틴저가날짜 이전일자 중 고가갱신이 있었던 경우
                                    highest_in_proc_under_martin_low_date = sorted(high_in_proc_under_martin_low_date.items(), key=lambda x: x[1], reverse=True)
                                        # 가격이 높은 순으로 정렬
                                    if highest_in_proc_under_martin_low_date[0][1] > tick_dict["price_for_cal"]["martin_high"]:
                                            #기존의 마틴고가보다 이후 마틴저가일자 이전 일자 최고가가 높은 경우
                                        tick_dict["price_for_cal"]["martin_high"] = highest_in_proc_under_martin_low_date[0][1]
                                        tick_dict["price_for_cal"]["martin_high_date"] = highest_in_proc_under_martin_low_date[0][0]
                                tick_dict["high_in_proc"] = {}
                            '''여기까지 마틴고가 갱신'''
                            tick_dict["result"][max(list(tick_dict["result"].keys()))+1]={
                                "martin_high":tick_dict["price_for_cal"]["martin_high"],
                                "martin_low":tick_dict["price_for_cal"]["martin_low"],
                                "martin_close":high_price,
                                "martin_high_date":tick_dict["price_for_cal"]["martin_high_date"],
                                "martin_low_date":tick_dict["price_for_cal"]["martin_low_date"],
                                "martin_close_date":_date,
                                "martin_drawdown":(tick_dict["price_for_cal"]["martin_high"] - tick_dict["price_for_cal"]["martin_low"])
                            }
                            tick_dict["price_for_cal"]["martin_high"]=high_price # 고가에서 청산됐다고 가정했으므로 새로운 마틴고가는 현재고가로 지정
                            tick_dict["price_for_cal"]["martin_high_date"]=_date
                            tick_dict["price_for_cal"]["martin_low"]=low_price
                            tick_dict["price_for_cal"]["martin_low_date"]=_date  # 이후 고가에서 저가로 하락했다고 가정했으므로 새로운 마틴저가는 현재저가로 지정
                            if (close_price - tick_dict["price_for_cal"]["martin_low"]) > (martin_tick*2): # 현재마틴저가로부터 현재종가가 마틴틱보다 큰경우(시->고->저->종->고->종 가정)
                                tick_dict["result"][max(list(tick_dict["result"].keys()))+1]={
                                    "martin_high":tick_dict["price_for_cal"]["martin_high"],
                                    "martin_low":tick_dict["price_for_cal"]["martin_low"],
                                    "martin_close":close_price,
                                    "martin_high_date":tick_dict["price_for_cal"]["martin_high_date"],
                                    "martin_low_date":tick_dict["price_for_cal"]["martin_low_date"],
                                    "martin_close_date":_date,
                                    "martin_drawdown":(tick_dict["price_for_cal"]["martin_high"] - tick_dict["price_for_cal"]["martin_low"])
                                }
                                # tick_dict["price_for_cal"]["martin_high"]=high_price # 종가에서 청산했다고 가정-> 이후 고가로 상승했다고 가정-> 마틴고가=고가
                                # tick_dict["price_for_cal"]["martin_high_date"]=_date
                                tick_dict["price_for_cal"]["martin_low"]=close_price # 이후 고가에서 종가로 하락했다고 가정했으므로 새로운 마틴저가는 종가로 지정
                                tick_dict["price_for_cal"]["martin_low_date"]=_date  
                        elif tick_dict["price_for_cal"]["martin_low"] > low_price: # 현재 데이터에서 아무런 청산없이 저가가 이전마틴저가보다 더 낮을 경우
                            tick_dict["price_for_cal"]["martin_low"]=low_price # 저가를 새로운 마틴저가로 지정
                            tick_dict["price_for_cal"]["martin_low_date"]=_date
                        if tick_dict["price_for_cal"]["martin_high"] < high_price: # 청산되기 전에 이전마틴고가보다 고가가 높은 경우 tick_dict["high_in_proc"]에 추가
                            tick_dict["high_in_proc"][_date] = high_price
                count_date += 1
                print("\r",f'reverse: {reverse}, date: {count_date}',end="          ")
            except:
                continue
        # result_dict = {} #논인버스결과 또는 인버스결과 모음 딕셔너리
        count_martin_tick = 0
        for martin_tick in list(each_tick_dict_copy.keys()) :
            martin_tick_result_dict = each_tick_dict_copy[martin_tick]["result"] #해당마틴틱의 모든결과 딕셔너리
            if len(martin_tick_result_dict) == 1 : # 해당마틴틱의 결과 초반 예시용 0번 밖에 없을 경우
                continue
            highest_dd = 0 #최대 dd 찾기 시작
            for result_n in list(martin_tick_result_dict.keys()):
                drawdown = martin_tick_result_dict[result_n]["martin_drawdown"]
                if highest_dd < drawdown :
                    highest_dd = drawdown #최대 dd 갱신
            martin_count = math.ceil(highest_dd / martin_tick)
            need_bal = martin_tick # 마틴틱별 필요한 자금 계산
            first_bal = martin_tick
            for r1 in range(0,martin_count):
                if r1 != 0 :
                    need_bal += need_bal + first_bal #마틴 10번, 마틴틱 1 가정 -> 2+6+14
            result_count = len(martin_tick_result_dict)-1
            profit = result_count*martin_tick
            result_csv.writerow({
                'martin_tick':martin_tick, 
                'reverse':reverse, 
                'drawdown':highest_dd, 
                'martin_count':martin_count, 
                'result_count':result_count,
                'need_bal':need_bal,
                'profit':profit
                })
            # new_result_data = ResultData(symbol=_symbol, martin_tick=martin_tick, reverse=reverse, drawdown=highest_dd, martin_count=martin_count, result_count=len(martin_tick_result_dict)-1, need_bal=need_bal)
            # result_dict[martin_tick] = highest_dd #최대 dd값을 최종 결과 딕셔너리[해당마틴틱] 값에 삽입
            # print(f'{_symbol} {round(martin_tick,1)} {reverse} {highest_dd} {martin_count} {len(martin_tick_result_dict)-1}')
            count_martin_tick+=1
            print("\r",f'reverse: {reverse}, martin_tick_count: {count_martin_tick}',end="          ")
        each_tick_dict_copy = {}
        # return result_dict
    new_result_csv = open(result_file_path, mode='w', newline='') #결과저장될 csv
    fieldnames = ['martin_tick', 'reverse','drawdown','martin_count','result_count','need_bal', 'profit'] #csv columns
    new_result_dict_writter = csv.DictWriter(new_result_csv, fieldnames=fieldnames) #딕셔너리형식으로 작성
    new_result_dict_writter.writeheader() #csv column 작성
    # non_inverse_result = main_cal(each_tick_dict, martin_tick_list, all_date_list, csv_dict_data, result_file_path, False, new_result_dict_writter)
    inverse_result = main_cal(each_tick_dict, martin_tick_list, all_date_list, csv_dict_data, result_file_path, True, new_result_dict_writter)
    new_result_csv.close()
    # inverse_result = main_cal(each_tick_dict, martin_tick_list, all_date_list, db_table, _symbol, True)

def cal_martin_2(csv_dict_data, all_date_list, result_file_path, min_max_tick_size) :
    min_tick = min_max_tick_size[0]
    max_tick = min_max_tick_size[1]
    # 여기서부터 각 틱 별로 each tick dict 구성
    each_tick_dict = {} #최소틱부터 최대틱까지 최소틱단위로 나누어서 과정 및 결과값 입력
    for _tick in range(1,int((max_tick/min_tick)+1)): #1틱 부터 1틱기준 최대틱까지
        tick_dict = {} #개별 틱에 들어갈 딕셔너리 구성
        tick_dict["price_for_cal"] = {
            "martin_high":0.0,
            "martin_low":0.0,
            "martin_high_date":0.0,
            "martin_low_date":0.0
            } #연산에 필요한 가격정보 [마틴 전 최고가, 마틴 중 최저가]
        tick_dict["high_in_proc"] = {} # 마틴저가 날짜와 상관없이 청산이전까지의 고가 모두 입력 -> 이후 마틴저가날짜보다 이전이면서 최고가 찾아서 마틴고가와 변경
        tick_dict["result"] = [0.0,0] # 각 마틴마다 얼마만큼 하락했었는지 결과 #마틴고가 마틴저가 마틴종가 각각 날짜 하락폭 포함
        each_tick_dict[_tick*min_tick] = tick_dict
        #여기까지 각 틱 별로 each tick dict 구성
    ''' 
    연산계획: 
    첫 데이터의 경우 
    마틴전 최고가=고가, 마틴중 최저가=저가, 마틴중 최종가=종가
    첫데이터 이후
    성공실패기준: (현재고가)-(이전 마틴중최저가) > 마틴틱*2, (현재종가)-(현재 마틴중최저가) > 마틴틱*2
    마틴게일 성공시: 마틴전 최고가= 직전종가와 현재고가 중 더 높은 가격, 마틴중 최저가=저가, 마틴최종가=종가
    마틴게일 실패시: 마틴전 최고가= 이전 마틴전최고가 그대로, 마틴중최저가=저가, 마틴최종가=종가
    '''
    martin_tick_list = list(each_tick_dict.keys())
    # all_date_list = all_date_list[-550:] #수정필요
    def main_cal(each_tick_dict, martin_tick_list, all_date_list, csv_dict_data, result_file_path, reverse, result_csv):
        '''여기서부터 상위함수 변수 카피'''
        each_tick_dict_copy = each_tick_dict.copy()
        count_date = 0
        for _date in all_date_list :#각 일자별 for
            symbol_this_date_data = csv_dict_data[_date] #해당심볼 해당일자 db데이터
            if reverse==True:
                    # 공매도 계산시 사용
                open_price = -symbol_this_date_data["open"]
                low_price = -symbol_this_date_data["high"]
                high_price = -symbol_this_date_data["low"]
                close_price = -symbol_this_date_data["close"]
                    # 가격을 뒤집으면 일일기준 저가->고가, 고가->저가 됨으로 서로 바꿔줌
            else:
                open_price = symbol_this_date_data["open"]
                high_price = symbol_this_date_data["high"]
                low_price = symbol_this_date_data["low"]
                close_price = symbol_this_date_data["close"]
            for martin_tick in martin_tick_list: # 여기서부터 각 마틴 틱마다 별도 계산
                tick_dict = each_tick_dict_copy[martin_tick] # 해당마틴틱 계산 및 결과 딕트
                if all_date_list[0] == _date : # 첫데이터의 경우
                    tick_dict["price_for_cal"]["martin_high"]=high_price
                    tick_dict["price_for_cal"]["martin_high_date"]=_date
                    if (close_price - low_price) > (martin_tick*2) : #저가를 찍고 종가갔는데 마틴청산한 경우
                        tick_dict["result"][1] += 1 #청산 횟수 +1
                        if (high_price-low_price) > tick_dict["result"][0] :
                            tick_dict["result"][0] = (high_price-low_price)
                        tick_dict["price_for_cal"]["martin_low"]=close_price
                        tick_dict["price_for_cal"]["martin_low_date"]=_date # 마틴청산 결과 삽입 후 마틴저가를 종가로 설정(시가->고가->저가->종가 가정)
                    else:
                        tick_dict["price_for_cal"]["martin_low"]=low_price
                        tick_dict["price_for_cal"]["martin_low_date"]=_date # 첫데이터에서 마틴청산이 없는 경우 마틴저가를 현재저가로 설정
                else: #첫데이터가 아닌경우
                    if (high_price - tick_dict["price_for_cal"]["martin_low"]) > (martin_tick*2): #이전마틴저가로 부터 현재고가가 마틴틱보다 큰경우(시가->고가 가정))
                        '''여기서부터 마틴고가 갱신'''
                        if len(tick_dict["high_in_proc"]) >0 : #첫 마틴고가 이후 고가갱신이 1번이라도 있었던 경우
                            high_in_proc_under_martin_low_date = dict(filter(lambda x: x[0]<=tick_dict["price_for_cal"]["martin_low_date"], tick_dict["high_in_proc"].items()))
                                # high_in_proc 딕셔너리 내 마틴저가 날짜보다 이전의 날짜 필터링
                            if len(high_in_proc_under_martin_low_date) >0 :#마틴저가날짜 이전일자 중 고가갱신이 있었던 경우
                                highest_in_proc_under_martin_low_date = sorted(high_in_proc_under_martin_low_date.items(), key=lambda x: x[1], reverse=True)
                                    # 가격이 높은 순으로 정렬
                                if highest_in_proc_under_martin_low_date[0][1] > tick_dict["price_for_cal"]["martin_high"]:
                                        #기존의 마틴고가보다 이후 마틴저가일자 이전 일자 최고가가 높은 경우
                                    tick_dict["price_for_cal"]["martin_high"] = highest_in_proc_under_martin_low_date[0][1]
                                    tick_dict["price_for_cal"]["martin_high_date"] = highest_in_proc_under_martin_low_date[0][0]
                            tick_dict["high_in_proc"] = {}
                        '''여기까지 마틴고가 갱신'''
                        tick_dict["result"][1] += 1 #청산 횟수 +1
                        if (tick_dict["price_for_cal"]["martin_high"] - tick_dict["price_for_cal"]["martin_low"]) > tick_dict["result"][0]:
                            tick_dict["result"][0] = tick_dict["price_for_cal"]["martin_high"] - tick_dict["price_for_cal"]["martin_low"]
                        tick_dict["price_for_cal"]["martin_high"]=high_price # 고가에서 청산됐다고 가정했으므로 새로운 마틴고가는 현재고가로 지정
                        tick_dict["price_for_cal"]["martin_high_date"]=_date
                        tick_dict["price_for_cal"]["martin_low"]=low_price
                        tick_dict["price_for_cal"]["martin_low_date"]=_date  # 이후 고가에서 저가로 하락했다고 가정했으므로 새로운 마틴저가는 현재저가로 지정
                        if (close_price - tick_dict["price_for_cal"]["martin_low"]) > (martin_tick*2): # 현재마틴저가로부터 현재종가가 마틴틱보다 큰경우(시->고->저->종->고->종 가정)
                            tick_dict["result"][1] += 1 #청산 횟수 +1
                            if tick_dict["result"][0] < (tick_dict["price_for_cal"]["martin_high"] - tick_dict["price_for_cal"]["martin_low"]) :
                                tick_dict["result"][0] = (tick_dict["price_for_cal"]["martin_high"] - tick_dict["price_for_cal"]["martin_low"])
                            # tick_dict["price_for_cal"]["martin_high"]=high_price # 종가에서 청산했다고 가정-> 이후 고가로 상승했다고 가정-> 마틴고가=고가
                            # tick_dict["price_for_cal"]["martin_high_date"]=_date
                            tick_dict["price_for_cal"]["martin_low"]=close_price # 이후 고가에서 종가로 하락했다고 가정했으므로 새로운 마틴저가는 종가로 지정
                            tick_dict["price_for_cal"]["martin_low_date"]=_date  
                    elif tick_dict["price_for_cal"]["martin_low"] > low_price: # 현재 데이터에서 아무런 청산없이 저가가 이전마틴저가보다 더 낮을 경우
                        tick_dict["price_for_cal"]["martin_low"]=low_price # 저가를 새로운 마틴저가로 지정
                        tick_dict["price_for_cal"]["martin_low_date"]=_date
                    if tick_dict["price_for_cal"]["martin_high"] < high_price: # 청산되기 전에 이전마틴고가보다 고가가 높은 경우 tick_dict["high_in_proc"]에 추가
                        tick_dict["high_in_proc"][_date] = high_price
            count_date += 1
            print("\r",f'reverse: {reverse}, date: {count_date}',end="          ")
        # result_dict = {} #논인버스결과 또는 인버스결과 모음 딕셔너리
        count_martin_tick = 0
        for martin_tick in list(each_tick_dict_copy.keys()) :
            martin_tick_result_list = each_tick_dict_copy[martin_tick]["result"] #해당마틴틱의 모든결과 딕셔너리
            if martin_tick_result_list[1] == 0 : # 해당마틴틱의 결과 초반 예시용 0번 밖에 없을 경우
                continue
            highest_dd = martin_tick_result_list[0] #최대 dd 찾기 시작
            martin_count = math.ceil(highest_dd / martin_tick)
            need_bal = martin_tick # 마틴틱별 필요한 자금 계산
            first_bal = martin_tick
            for r1 in range(0,martin_count):
                if r1 != 0 :
                    need_bal += need_bal + first_bal #마틴 10번, 마틴틱 1 가정 -> 2+6+14
            result_count = martin_tick_result_list[1]
            profit = result_count*martin_tick
            result_csv.writerow({
                'martin_tick':martin_tick, 
                'reverse':reverse, 
                'drawdown':highest_dd, 
                'martin_count':martin_count, 
                'result_count':result_count,
                'need_bal':need_bal,
                'profit':profit
                })
            # new_result_data = ResultData(symbol=_symbol, martin_tick=martin_tick, reverse=reverse, drawdown=highest_dd, martin_count=martin_count, result_count=len(martin_tick_result_dict)-1, need_bal=need_bal)
            # result_dict[martin_tick] = highest_dd #최대 dd값을 최종 결과 딕셔너리[해당마틴틱] 값에 삽입
            # print(f'{_symbol} {round(martin_tick,1)} {reverse} {highest_dd} {martin_count} {len(martin_tick_result_dict)-1}')
            count_martin_tick+=1
            print("\r",f'reverse: {reverse}, martin_tick_count: {count_martin_tick}',end="          ")
        each_tick_dict_copy = {}
        # return result_dict
    new_result_csv = open(result_file_path, mode='w', newline='') #결과저장될 csv
    fieldnames = ['martin_tick', 'reverse','drawdown','martin_count','result_count','need_bal', 'profit'] #csv columns
    new_result_dict_writter = csv.DictWriter(new_result_csv, fieldnames=fieldnames) #딕셔너리형식으로 작성
    new_result_dict_writter.writeheader() #csv column 작성
    non_inverse_result = main_cal(each_tick_dict, martin_tick_list, all_date_list, csv_dict_data, result_file_path, False, new_result_dict_writter)
    inverse_result = main_cal(each_tick_dict, martin_tick_list, all_date_list, csv_dict_data, result_file_path, True, new_result_dict_writter)
    new_result_csv.close()
    # inverse_result = main_cal(each_tick_dict, martin_tick_list, all_date_list, db_table, _symbol, True)

########################메인프로세싱########################
start_t = time.time() #타이머시작
if not os.path.isdir(data_folder):
    raise print("no data files") #데이터 폴더가 없으면 종료
if not os.path.isdir(result_folder):
    os.mkdir(result_folder) #결과 폴더가 없으면 폴더 제작
data_file_list = os.listdir(data_folder)
count_symbol_n = [0]
def multiprocessing_work(data_file_list):
    for data_file in data_file_list: #여기서부터 계산시작
        result_file_path = f'{result_folder}/{data_file}'#수정필요
        data_file_path = f'{data_folder}/{data_file}'
        if os.path.isfile(result_file_path):
            continue #이미 결과파일이 존재하는 경우 패스
        symbol_data = open(data_file_path, encoding="utf-8", mode="r") # csv 파일열기
        symbol_data_dict_reader = csv.DictReader(symbol_data) #csv파일dict reader로 열기
        symbol_data_dict = (lambda x: {i["Date"]:{"open":float(i["Open"]),"high":float(i["High"]),"low":float(i["Low"]),"close":float(i["Close"])} for i in x})(symbol_data_dict_reader)
        all_date_list = list(symbol_data_dict.keys()) #수정필요
        min_max_tick_size_list = find_min_max_tick_size(symbol_data_dict, all_date_list) # [min_tick_size, max_tick_size]
        martin_result = cal_martin_2(symbol_data_dict, all_date_list, result_file_path, min_max_tick_size_list) #마틴게일 최적화 계산
        count_symbol_n[0] += 1
        symbol_data.close()
        print("\r",f'symbol count: {str(count_symbol_n[0]).zfill(6)}  symbol: {data_file}          ')
# num_cores = multiprocessing.cpu_count() # 8
# chunk_size = math.ceil(len(data_file_list) / num_cores)
# splited_symbol_list = [data_file_list[i * chunk_size:(i + 1) * chunk_size] for i in range((len(data_file_list) - 1 + chunk_size) // chunk_size )] 
# multi_work = parmap.map(multiprocessing_work, splited_symbol_list, pm_pbar=True, pm_processes=num_cores)
# multiprocessing_work(data_file_list) 
# multiprocessing_work(["AUS200,None.csv"]) #수정필요
multiprocessing_work(["EURUSD,None.csv"]) #수정필요
    

print(f'used time: {time.time()-start_t}') #타이머 끝
